import axios from 'axios';

export async function getGithubUsername(githubID: string) {
  try {
    const { data } = await axios.get(
      `https://api.github.com/user/${githubID}`,
      {
        headers: {
          Accept: 'application/json',
        },
      }
    );

    return data.login;
  } catch (error) {
    if (axios.isAxiosError(error)) {
      console.log('error message: ', error.message);
      return error.message;
    } else {
      console.log('unexpected error: ', error);
      return 'An unexpected error occurred';
    }
  }
}

export function spliceGithubID(githubImageURL: string) {
  const startIndex = githubImageURL.indexOf('u/') + 2;
  const endIndex = startIndex + 8;
  if (githubImageURL[endIndex] === '?') {
    return githubImageURL.substring(startIndex, endIndex);
  } else {
    return githubImageURL.substring(startIndex, endIndex + 1);
  }
}
