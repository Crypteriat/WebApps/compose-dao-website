import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import siteMetadata from '@data/siteMetadata';
import { PageSeo } from '@components/SEO';
import FadeIn from 'react-fade-in';

export async function getStaticProps({
  locale = Object.keys(siteMetadata.locales[0])[0],
}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'index', 'footer'])),
    },
  };
}

export default function Home() {
  const { t } = useTranslation('index');

  return (
    <>
      <PageSeo
        title={siteMetadata.title}
        description={siteMetadata.description}
        url={siteMetadata.siteUrl}
      />
      <FadeIn delay={200} transitionDuration={1500}>
        <div className="text-center divide-y pt-8 divide-gray-200 dark:divide-gray-700">
          <h1 className="text-2xl sm:text-md leading-2 dark:text-gray-300">
            {t('header')}
          </h1>
        </div>

        <div className="pt-6 text-center dark:text-gray-200">
          <p className="italic pb-4">{t('description_p1')}</p>
          <p className="italic pb-4">{t('description_p2')}</p>
          <p className="italic pb-4">{t('description_p3')}</p>
        </div>

        <div className="text-center pt-4">
          <a
            href={siteMetadata.documentation}
            target="_blank"
            className="text-2xl sm:text-md leading-2 dark:text-gray-300 hover:text-blue-400 dark:hover:text-blue-400"
          >
            {t('documentation')}
          </a>
        </div>
      </FadeIn>
    </>
  );
}
