import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useEffect, useState } from 'react';
import { useSession } from 'next-auth/react';
import FadeIn from 'react-fade-in';
import siteMetadata from '@data/siteMetadata';
import { PageSeo } from '@components/SEO';
import GithubButton from '@components/GithubButton';
import Link from '@components/Link';
import { getGithubUsername, spliceGithubID } from './api/github';

export async function getStaticProps({
  locale = Object.keys(siteMetadata.locales[0])[0],
}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'discord',
        'footer',
        'github',
      ])),
    },
  };
}

export default function Home() {
  const { t } = useTranslation('discord');
  const { data: session, status } = useSession();
  const [githubUsername, setGithubUsername] = useState('');
  const [ephemeralLink, setEphemeralLink] = useState('');
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    async function getUsername(githubID: string) {
      const username = await getGithubUsername(githubID);
      setGithubUsername(username);
      await new Promise((resolve) => setTimeout(resolve, 10000));
    }

    async function getEphemeralLink(githubUsername: string) {
      fetch(
        `https://trpc-server.deno.dev/trpc/ephemeralLink?input=${encodeURIComponent(
          JSON.stringify(githubUsername)
        )}`,
        {
          headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          },
          mode: 'cors',
        }
      )
        .then((result) => result.json())
        .then((response) => {
          const link = response.result.data.link;
          setEphemeralLink(link);
          setIsLoaded(true);
        })
        .catch(console.error);
    }

    const githubImageURL = session ? session.user?.image : '';
    if (githubImageURL) {
      const githubID = spliceGithubID(githubImageURL);
      getUsername(githubID);
      if (githubUsername) {
        getEphemeralLink(githubUsername);
      }
    }
  }, [status, githubUsername]);

  return (
    <>
      <PageSeo
        title={siteMetadata.title}
        description={siteMetadata.description}
        url={siteMetadata.siteUrl}
      />
      <FadeIn delay={200} transitionDuration={1500}>
        <div className="text-center divide-y pt-4 divide-gray-200 dark:divide-gray-700">
          <h1 className="text-2xl sm:text-md leading-2 dark:text-gray-300">
            {t('header')}
          </h1>
        </div>

        <div className="py-7 text-center text-md dark:text-gray-300">
          {t('githubSignin')}
        </div>

        <GithubButton></GithubButton>
        {session ? (
          <>
            <div className="text-center pt-12 px-20">
              <p className="text-md dark:text-gray-300">{t('redirectMsg')}</p>
            </div>
            <div className="pt-10 max-w-xs px-20 mx-auto">
              {isLoaded ? (
                <>
                  <Link
                    href={ephemeralLink}
                    aria-label="discord_verification_link"
                  >
                    <div className="bg-indigo-400 dark:bg-indigo-900 dark:hover:bg-blue-400 hover:bg-blue-400 text-sm font-bold uppercase py-2 rounded text-center">
                      {t('launchButton')}
                    </div>
                  </Link>
                </>
              ) : (
                <div className="bg-gray-500 text-sm font-bold uppercase py-2 rounded text-center cursor-not-allowed">
                  {t('loading')}
                </div>
              )}
            </div>
          </>
        ) : (
          <>
            <div className="flex flex-col">
              <p className="pt-20 pb-7 text-center text-md dark:text-gray-300">
                {t('checkThingsOut')}
              </p>
              <a
                className="bg-indigo-900 hover:bg-blue-600 text-sm font-bold uppercase p-2 px-12 rounded text-center mx-auto"
                href={siteMetadata.discord}
                target="_blank"
              >
                {t('join')}
              </a>
            </div>
          </>
        )}
      </FadeIn>
    </>
  );
}
