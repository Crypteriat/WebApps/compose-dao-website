import siteMetadata from '@data/siteMetadata';
import headerNavLinks from '@data/headerNavLinks';
import Logo from '@data/logo.svg';
import Link from './Link.js';
import SectionContainer from './SectionContainer.js';
import Footer from './Footer';
import MobileNav from './MobileNav.js';
import ThemeSwitch from './ThemeSwitch';
import SelectLocale from './SelectLocale';
import { useTranslation } from 'next-i18next';

function LayoutWrapper({ children }) {
  const { t } = useTranslation('common');

  return (
    <SectionContainer>
      <div className="flex flex-col justify-between h-screen">
        <header className="flex items-center justify-between py-0 md:py-6 lg:py-10 ">
          <Link href="/" aria-label="ComposeDAO">
            <div className="flex items-center">
              <Logo />
              <div className="pl-4">
                {typeof siteMetadata.headerTitle === 'string' ? (
                  <span className="hidden text-2xl font-semibold text-center md:text-1xl lg:mb-0 md:mb-0 sm:block">
                    {siteMetadata.headerTitle}
                  </span>
                ) : (
                  siteMetadata.headerTitle
                )}
              </div>
            </div>
          </Link>
          <div className="inline-flex mx-auto sm:pl-40 md:pl-20">
            <SelectLocale />
          </div>
          <div className="flex items-center text-base">
            <div className="hidden sm:block">
              {headerNavLinks.map((link) => (
                <Link
                  key={link.title}
                  href={link.href}
                  className="p-1 font-medium text-gray-900 sm:p-4 dark:text-gray-100 hover:text-blue-400 dark:hover:text-blue-400"
                >
                  {t(`headerNavLinks.${link.title}`)}
                </Link>
              ))}
            </div>
            <ThemeSwitch />
            <MobileNav />
          </div>
        </header>
        <main className="mb-auto">{children}</main>
        <Footer />
      </div>
    </SectionContainer>
  );
}

export default LayoutWrapper;
