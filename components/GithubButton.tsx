import { useTranslation } from 'next-i18next';
import { signIn, signOut, useSession } from 'next-auth/react';
import styles from '../css/header.module.css';

// The approach used in this component shows how to build a sign in and sign out
// component that works on pages which support both client and server side
// rendering, and avoids any flash incorrect content on initial page load.
export default function GithubButton() {
  const { t } = useTranslation('github');
  const { data: session, status } = useSession();
  const loading = status === 'loading';

  return (
    <div className={styles.signedInStatus}>
      <p
        className={`nojs-show ${
          !session && loading ? styles.loading : styles.loaded
        }`}
      >
        {!session && (
          <>
            <span className={styles.notSignedInText}>{t('notSignedIn')}</span>
            <a
              href={`/api/auth/signin`}
              className={styles.buttonPrimary}
              onClick={(e) => {
                e.preventDefault();
                signIn('github');
              }}
            >
              {t('signin')}
            </a>
          </>
        )}
        {session?.user && (
          <>
            {session.user.image && (
              <span
                style={{ backgroundImage: `url('${session.user.image}')` }}
                className={styles.avatar}
              />
            )}
            <span className={styles.signedInText}>
              <small>{t('signedin')}</small>
              <br />
              <strong>{session.user.email ?? session.user.name}</strong>
            </span>
            <a
              href={`/api/auth/signout`}
              className={styles.button}
              onClick={(e) => {
                e.preventDefault();
                signOut();
              }}
            >
              {t('signout')}
            </a>
          </>
        )}
      </p>
    </div>
  );
}
