import {NextSeo, ArticleJsonLd} from 'next-seo';
import siteMetadata from '@data/siteMetadata';
import PropTypes from 'prop-types';

export const SEO = {
  title: siteMetadata.title,
  description: siteMetadata.description,
  openGraph: {
    type: 'website',
    locale: siteMetadata.language,
    url: siteMetadata.siteUrl,
    title: siteMetadata.title,
    description: siteMetadata.description,
    images: [
      {
        url: `${siteMetadata.siteUrl}${siteMetadata.socialBanner}`,
        alt: siteMetadata.title,
        width: 1200,
        height: 600,
      },
    ],
  },
  twitter: {
    handle: siteMetadata.twitter,
    site: siteMetadata.twitter,
    cardType: 'summary_large_image',
  },
  additionalMetaTags: [
    {
      name: 'author',
      content: siteMetadata.author,
    },
  ],
};

export const PageSeo = ({title, description, url}) => (
  <NextSeo
    title={`${title} – ${siteMetadata.title}`}
    description={description}
    canonical={url}
    openGraph={{
      url,
      title,
      description,
    }}
  />
);

PageSeo.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export const MeetingSeo = ({
  title,
  summary,
  date,
  lastmod,
  url,
  tags,
  images = [],
}) => {
  const publishedAt = new Date(date).toISOString();
  const modifiedAt = new Date(lastmod || date).toISOString();
  const imagesArray
    = images.length === 0
      ? [siteMetadata.socialBanner]
      : (typeof images === 'string'
        ? [images]
        : images);

  const featuredImages = imagesArray.map(img => ({
    url: `${siteMetadata.siteUrl}${img}`,
    alt: title,
  }));

  return (
    <>
      <NextSeo
        title={`${title} – ${siteMetadata.title}`}
        description={summary}
        canonical={url}
        openGraph={{
          type: 'article',
          article: {
            publishedTime: publishedAt,
            modifiedTime: modifiedAt,
            authors: [`${siteMetadata.siteUrl}/about`],
            tags,
          },
          url,
          title,
          description: summary,
          images: featuredImages,
        }}
        additionalMetaTags={[
          {
            name: 'twitter:image',
            content: featuredImages[0].url,
          },
        ]}
      />
      <ArticleJsonLd
        authorName={siteMetadata.author}
        dateModified={modifiedAt}
        datePublished={publishedAt}
        description={summary}
        publisherLogo="featuredImages[0].alt"
        images={featuredImages.map((elm)=> elm.url )}
        publisherName={siteMetadata.author}
        title={title}
        url={url}
      />
    </>
  );
};

MeetingSeo.propTypes = {
  title: PropTypes.string.isRequired,
  summary: PropTypes.string.isRequired,
  // Date: PropTypes.string.isRequired,
  // lastmod: PropTypes.string.isRequired
  url: PropTypes.string.isRequired,
  tags: PropTypes.array,
  images: PropTypes.array,
};
